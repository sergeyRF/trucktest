package com.stalincrew.horoscope.truckstest

import android.app.Application
import com.stalincrew.horoscope.truckstest.di.myModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class App:Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(myModule))

        Timber.plant(Timber.DebugTree())
    }
}
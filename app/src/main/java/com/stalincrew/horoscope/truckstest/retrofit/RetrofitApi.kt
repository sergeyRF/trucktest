package com.stalincrew.horoscope.truckstest.retrofit

import com.stalincrew.horoscope.truckstest.Truck
import io.reactivex.Observable
import retrofit2.http.*

interface RetrofitApi {

    companion object {
        const val BASE_URL = "http://rsprm.ru/test/"

    }

    @GET("trucks")
    fun loadTracks(): Observable<List<Truck>>

    @POST("truck/add")
    fun saveTrack(@Body newTruck: Truck): Observable<Truck>

    @DELETE("truck/{id}")
    fun deleteTruck(@Path("id") id: String): Observable<String>

    @PATCH("truck/{id}")
    fun editTruck(@Path("id") id: String, @Body editTruck: Truck): Observable<Truck>


}
package com.stalincrew.horoscope.truckstest.extentions

import android.view.View

fun View.setVisibility(visible: Boolean) {
    visibility = if(visible) View.VISIBLE else View.GONE
}
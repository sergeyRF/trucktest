package com.stalincrew.horoscope.truckstest.retrofit

import com.stalincrew.horoscope.truckstest.Truck
import io.reactivex.Observable

class TruckProviderImpl(
        private val retrofit: RetrofitApi
) : TruckProvider {
    override fun getTrucks(): Observable<List<Truck>> {
        return retrofit.loadTracks()
                .map { trucks ->
                    trucks.filter { it.nameTruck != null && it.comment != null && it.price != null }
                }
    }

    override fun createTruck(truck: Truck): Observable<Truck> {
        return retrofit.saveTrack(truck)

    }

    override fun editTruck(truck: Truck): Observable<Truck> {
        return retrofit.editTruck(
                truck.id.toString(),
                truck
        )
    }

    override fun deleteTruck(id: Int): Observable<String> {
        return retrofit.deleteTruck(id.toString())
    }
}
package com.stalincrew.horoscope.truckstest.screens.edit

import android.content.Context
import androidx.lifecycle.ViewModel
import com.stalincrew.horoscope.truckstest.R
import com.stalincrew.horoscope.truckstest.Truck
import com.stalincrew.horoscope.truckstest.retrofit.TruckProvider
import com.stalincrew.horoscope.truckstest.utils.SingleLiveEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.SerialDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class EditViewModel(
    private val provider: TruckProvider,
    private val context: Context
) : ViewModel() {

    val editTruckCompletedLD = SingleLiveEvent<String>()
    val onBackLD = SingleLiveEvent<Boolean>()

    private val disposable = SerialDisposable()


    fun editTruck(id: Int, name: String, price: String, comment: String) {

        if (name.isNotEmpty() && comment.isNotEmpty() && price.isNotEmpty()) {
            if (id == 0) {
                truckCreated(provider.createTruck(Truck(id, name, price.toFloat(), comment)))
            } else {
                truckCreated(provider.editTruck(Truck(id, name, price.toFloat(), comment)))
            }
        } else {
            editTruckCompletedLD.value = context.getString(R.string.not_fulled)
        }
    }

    private fun truckCreated(observable: Observable<Truck>) {
        observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                editTruckCompletedLD.value = context.getString(R.string.sucsessful)
                onBackLD.value = true
            }, {
                Timber.e(it)
                editTruckCompletedLD.value = context.getString(R.string.error_loading)
            }).apply { disposable.set(this) }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}
package com.stalincrew.horoscope.truckstest.retrofit

import com.stalincrew.horoscope.truckstest.Truck
import io.reactivex.Observable

interface TruckProvider {

    fun getTrucks(): Observable<List<Truck>>
    fun createTruck(truck: Truck): Observable<Truck>
    fun editTruck(truck: Truck): Observable<Truck>
    fun deleteTruck(id: Int): Observable<String>
}
package com.stalincrew.horoscope.truckstest.screens.trucks

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stalincrew.horoscope.truckstest.R
import com.stalincrew.horoscope.truckstest.Truck
import com.stalincrew.horoscope.truckstest.retrofit.TruckProvider
import com.stalincrew.horoscope.truckstest.screens.edit.EditTruckFragment
import com.stalincrew.horoscope.truckstest.utils.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class TruckViewModel(
        private val provider: TruckProvider,
        private val context: Context
) : ViewModel() {

    val trucksLiveData = MutableLiveData<List<Truck>>()
    val startFragmentLD = SingleLiveEvent<Fragment>()

    val loadingLiveData = MutableLiveData<Boolean>()
    val errorLiveData = SingleLiveEvent<String>()
    val showRetryLiveData = MutableLiveData<Boolean>()

    private val disposable = CompositeDisposable()

    init {
        loadTruck()
    }

    fun loadTruck() {
        provider.getTrucks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    loadingLiveData.value = true
                    showRetryLiveData.value = false
                }
                .doFinally { loadingLiveData.value = false }
                .subscribe({ trucks ->
                    trucksLiveData.value = trucks
                }, {
                    errorLiveData.value = context.getString(R.string.error_loading)
                    showRetryLiveData.value = true
                    Timber.e(it)
                }).apply { disposable.add(this) }
    }


    fun deleteTruck(truck: Truck) {
        provider.deleteTruck(truck.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ loadTruck() }, { Timber.e(it) }).apply { disposable.add(this) }
    }


    fun fragmentCreateTruck() {
        startFragmentLD.value = EditTruckFragment.getInstance(null)
    }

    fun fragmentEditTruck(truck: Truck) {
        startFragmentLD.value = EditTruckFragment
                .getInstance(truck)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

}
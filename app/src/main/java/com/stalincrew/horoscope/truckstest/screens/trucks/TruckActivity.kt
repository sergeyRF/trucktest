package com.stalincrew.horoscope.truckstest.screens.trucks

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.stalincrew.horoscope.truckstest.R
import com.stalincrew.horoscope.truckstest.extentions.observeSafe
import org.koin.androidx.viewmodel.ext.android.viewModel

class TruckActivity : AppCompatActivity() {

    private val viewModel by viewModel<TruckViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_truck)

        if (supportFragmentManager.findFragmentById(R.id.container) == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, TrucksFragment())
                    .commit()
        }

        viewModel.startFragmentLD.observeSafe(this) {
            startFragment(it)
        }

    }

    private fun startFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
    }
}

package com.stalincrew.horoscope.truckstest.screens.trucks.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.stalincrew.horoscope.truckstest.R
import com.stalincrew.horoscope.truckstest.Truck

class RvAdapter : RecyclerView.Adapter<RvAdapter.Holder>() {

    private var listTruck = listOf<Truck>()

    var onClick: (Truck) -> Unit = {}

    var onDelete: (Truck) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.holder_truck, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return listTruck.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(listTruck[position])
    }

    fun setTrucks(trucks: List<Truck>) {
        listTruck = trucks
        notifyDataSetChanged()
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {

        private val name = view.findViewById<TextView>(R.id.tvNameTruck)
        private val price = view.findViewById<TextView>(R.id.tvPrice)
        private val comment = view.findViewById<TextView>(R.id.tvComment)
        private val cvTruck = view.findViewById<CardView>(R.id.cvTruck)
        private val delete = view.findViewById<ImageView>(R.id.ivDeleteTruck)

        fun bind(truck: Truck) {
            name.text = truck.nameTruck
            price.text = truck.price.toString()
            comment.text = truck.comment

            cvTruck.setOnClickListener {
                onClick.invoke(truck)
            }

            delete.setOnClickListener {
                onDelete.invoke(truck)
            }
        }
    }
}
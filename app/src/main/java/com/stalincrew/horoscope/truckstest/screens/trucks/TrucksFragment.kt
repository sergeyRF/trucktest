package com.stalincrew.horoscope.truckstest.screens.trucks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.stalincrew.horoscope.truckstest.R
import com.stalincrew.horoscope.truckstest.extentions.observeSafe
import com.stalincrew.horoscope.truckstest.extentions.setVisibility
import com.stalincrew.horoscope.truckstest.screens.trucks.adapter.RvAdapter
import kotlinx.android.synthetic.main.fragment_trucks.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class TrucksFragment : Fragment() {

    private val viewModel by sharedViewModel<TruckViewModel>()

    private val adapter = RvAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trucks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvTrucks.layoutManager = LinearLayoutManager(context)
        rvTrucks.adapter = adapter

        viewModel.trucksLiveData.observeSafe(this) { trucks ->
            adapter.setTrucks(trucks)
        }

        adapter.onClick = {
            viewModel.fragmentEditTruck(it)
        }

        adapter.onDelete = {
            viewModel.deleteTruck(it)
        }

        fabAddTruck.setOnClickListener {
            viewModel.fragmentCreateTruck()
        }

        viewModel.loadingLiveData.observe(this, Observer { visible ->
            progress.setVisibility(visible)
        })

        viewModel.showRetryLiveData.observe(this, Observer { visible ->
            if (visible){
                adapter.setTrucks(listOf())
            }
            rvTrucks.setVisibility(!visible)
            tvReload.setVisibility(visible)
        })

        viewModel.errorLiveData.observe(this, Observer { error ->
            Toast.makeText(context, error, Toast.LENGTH_LONG).show()
        })

        tvReload.setOnClickListener {
            viewModel.loadTruck()
        }

        pullToRefresh.setOnRefreshListener {
            viewModel.loadTruck()
            pullToRefresh.isRefreshing = false
        }
    }

}
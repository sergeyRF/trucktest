package com.stalincrew.horoscope.truckstest.screens.edit

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.stalincrew.horoscope.truckstest.R
import com.stalincrew.horoscope.truckstest.Truck
import com.stalincrew.horoscope.truckstest.extentions.observeSafe
import com.stalincrew.horoscope.truckstest.screens.trucks.TruckViewModel
import kotlinx.android.synthetic.main.fragment_edit_truck.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class EditTruckFragment : Fragment() {

    companion object {
        private const val EXTRA_TRUCK = "extra_truck"
        fun getInstance(truck: Truck?) = EditTruckFragment().apply {
            arguments = Bundle().apply {
                putSerializable(EXTRA_TRUCK, truck)
            }
        }
    }

    private val truck: Truck? by lazy { arguments?.getSerializable(EXTRA_TRUCK) as Truck? }

    private val viewModel by viewModel<EditViewModel>()
    private val viewModelTruck by sharedViewModel<TruckViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_edit_truck, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        truck?.let {
            setEditTruck(it)
        }

        viewModel.editTruckCompletedLD.observeSafe(this) {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        }

        viewModel.onBackLD.observeSafe(this) {
            viewModelTruck.loadTruck()
            activity?.onBackPressed()
        }
    }

    private fun setEditTruck(truck: Truck) {
        etName.setText(truck.nameTruck)
        etPrice.setText(truck.price.toString())
        etComment.setText(truck.comment)
    }

    private fun saveTruck() {
        viewModel.editTruck(
            truck?.id ?: 0,
            etName.text.toString(),
            etPrice.text.toString(),
            etComment.text.toString()
        )
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as AppCompatActivity).apply {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_edit_truck, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_et_save) {
            saveTruck()
        }
        return super.onOptionsItemSelected(item)
    }
}
package com.stalincrew.horoscope.truckstest.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.stalincrew.horoscope.truckstest.retrofit.RetrofitApi
import com.stalincrew.horoscope.truckstest.retrofit.TruckProvider
import com.stalincrew.horoscope.truckstest.retrofit.TruckProviderImpl
import com.stalincrew.horoscope.truckstest.screens.edit.EditViewModel
import com.stalincrew.horoscope.truckstest.screens.trucks.TruckViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val myModule = module {

    viewModel { TruckViewModel(get(),get()) }
    viewModel { EditViewModel(get(),get()) }
    single { getGson() }
    single { getRetrofit() }
    single { getProvider(get()) }

}

private fun getProvider(
        retrofitApi: RetrofitApi
): TruckProvider {
    return TruckProviderImpl(retrofitApi)
}

private fun getRetrofit(): RetrofitApi {

    val retrofit = Retrofit.Builder()
            .client(client())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(RetrofitApi.BASE_URL)
            .build()
    return retrofit.create(RetrofitApi::class.java)
}

private fun client() =
        OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

private fun getGson(): Gson = GsonBuilder().create()
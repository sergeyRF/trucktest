package com.stalincrew.horoscope.truckstest

import java.io.Serializable

data class Truck(
        val id: Int,
        val nameTruck: String?,
        val price: Float?,
        val comment: String?
):Serializable


